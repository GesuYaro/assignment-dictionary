%define IS_EQUAL 1

global find_word
%include "lib.inc"

section .text
; rdi - pointer to string, that is needed to find
; rsi - pointer to the start of the dictionary
find_word:
    .loop:
        test rsi, rsi ; check if pointer to the next block is null
		je .fail ; if it is, dictionary end

        push rdi
        push rsi
        add rsi, 8 ; move pointer to the key
        call string_equals
        pop rsi
        pop rdi

        cmp rax, IS_EQUAL
        je .success

        mov rsi, [rsi] ; move pointer to the next block
		jmp .loop

    .success:
        mov rax, rsi
		ret

    .fail:
        xor rax, rax
        ret