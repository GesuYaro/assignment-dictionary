%macro colon 2
    %ifdef CURR_FIRST_ELEM
        %2: dq CURR_FIRST_ELEM
    %else
        %2: dq 0
    %endif
    db %1, 0
    %define CURR_FIRST_ELEM %2
%endmacro