%.o: %.asm
	nasm -g -felf64 $< -o $@

main.o: words.inc colon.inc main.asm
	nasm -felf64 main.asm -o $@

program: main.o lib.o dict.o
	ld -o program $^
