%define BUFFER_SIZE 256

%include "colon.inc"

section .rodata
%include "words.inc"

error_message: db "invalid key", 0x0A, 0
not_found_message: db "nothing found for this key", 0x0A, 0

section .bss
buffer: resb BUFFER_SIZE

section .text
global _start
%include "lib.inc"
extern find_word

_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_string
    test rax, rax
    jz .error

    push rdx
    mov rdi, buffer
    mov rsi, CURR_FIRST_ELEM
    call find_word
    test rax, rax
    jz .not_found
    pop rdx
    lea rax, [rax + 8 + rdx + 1]
    mov rdi, rax
    jmp .print_result

    .error:
        mov rdi, error_message 
        call print_error
        jmp .exit

    .not_found:
        mov rdi, not_found_message
        call print_error
        jmp .exit

    .print_result:
        call print_string
        call print_newline
        jmp .exit

    .exit:
        call exit

